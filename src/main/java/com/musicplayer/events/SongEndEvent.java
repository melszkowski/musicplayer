package com.musicplayer.events;

public class SongEndEvent extends VisitableEvent
{
    @Override
    public void accept(EventVisitor visitor)
    {
        visitor.visit(this);
    }

    @Override
    public String toString()
    {
        return "SongEndEvent{}";
    }
}
