package com.musicplayer.events;

public interface EventVisitor
{
    void visit(MusicPlayEvent event);

    void visit(MusicPauzeEvent event);

    void visit(SoundOnEvent event);

    void visit(SoundOffEvent event);

    void visit(ShuffleOffEvent shuffleOffEvent);

    void visit(ShuffleOnEvent shuffleOnEvent);

    void visit(RepeatOffEvent repeatOffEvent);

    void visit(RepeatOnEvent repeatOnEvent);

    void visit(SongEndEvent songEndEvent);

    void visit(MusicStopEvent musicStopEvent);

    void visit(NoneEvent noneEvent);
}
