package com.musicplayer.events;

public class MusicStopEvent extends VisitableEvent
{
    @Override
    public void accept(EventVisitor visitor)
    {
        visitor.visit(this);
    }

    @Override
    public String toString()
    {
        return "MusicStopEvent{}";
    }
}
