package com.musicplayer.events;

public interface Observer
{
    public void notify(VisitableEvent event);

}
