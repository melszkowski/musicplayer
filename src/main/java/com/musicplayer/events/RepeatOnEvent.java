package com.musicplayer.events;

public class RepeatOnEvent extends VisitableEvent
{
    @Override
    public void accept(EventVisitor visitor)
    {
        visitor.visit(this);
    }

    @Override
    public String toString()
    {
        return "RepeatOnEvent{}";
    }
}
