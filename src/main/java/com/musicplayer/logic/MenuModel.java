package com.musicplayer.logic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MenuModel
{

    //to be displayed in corner of the main screen
    public static String getCurrentTimeAsText()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date).toString();
    }
}
