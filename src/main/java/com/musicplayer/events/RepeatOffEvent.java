package com.musicplayer.events;

public class RepeatOffEvent extends VisitableEvent
{
    @Override
    public void accept(EventVisitor visitor)
    {
        visitor.visit(this);
    }

    @Override
    public String toString()
    {
        return "RepeatOffEvent{}";
    }
}
