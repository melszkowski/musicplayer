package com.musicplayer.controller;

import com.musicplayer.logic.MenuModel;
import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class MainController implements Initializable
{

    private MenuModel menuModel;
    private Stage currentStage;
    @FXML
    private Label currentTimeLabel;

    public MainController()
    {
        menuModel = new MenuModel();
    }

    public void setView(String fxmlPath)
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxmlPath));

        Parent root = null;
        try
        {
            root = (Parent) fxmlLoader.load();
        } catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }

        currentStage.getScene().setRoot(root);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        setCurrentTime();

        Observable.interval(1, TimeUnit.SECONDS).subscribe(second ->
        {
            Platform.runLater( () -> setCurrentTime());
        });
    }

    public void setStage(Stage s)
    {
        currentStage = s;
    }

    private void setCurrentTime()
    {
        currentTimeLabel.setText(MenuModel.getCurrentTimeAsText());
    }

    @FXML
    protected void handleButtonPressed(ActionEvent actionEvent)
    {
        Button button = (Button) actionEvent.getSource();
        String chosenOption = button.getText();

        changeViewByOption(chosenOption);

    }

    private void changeViewByOption(String chosenOption)
    {

        System.out.println("User chose: " + chosenOption);
        switch (chosenOption)
        {
            case "Music":
                System.out.println("MUSIC");
                setView("/ui/playerScreen.fxml");
                break;
            case "Radio":
                System.out.println("RADIO");
                break;
            case "GPS":
                System.out.println("GPS");
                break;
            case "Settings":
                System.out.println("SETTINGS");
                break;
        }

    }


}
