package com.musicplayer.events;

public class SoundOnEvent extends VisitableEvent
{
    @Override
    public void accept(EventVisitor visitor)
    {
        visitor.visit(this);
    }

    @Override
    public String toString()
    {
        return "SoundOnEvent{}";
    }
}
