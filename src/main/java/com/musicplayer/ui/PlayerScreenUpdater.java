package com.musicplayer.ui;

import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;

public class PlayerScreenUpdater
{

    public static final double SMALL_BUTTON_SIZE = 26.0;

    public void handleTogglePlayButton(Button togglePlayButton, boolean isPlaying)
    {
        StringBuilder newIconPath = isPlaying ? new StringBuilder("pause.png") : new StringBuilder("play.png");

        Image image = new Image(getClass().getResourceAsStream(newIconPath.insert(0, "/ui/icons/").toString()));
        updateButtonImage(togglePlayButton, image);

    }

    public void handleToggleMuteButton(Button toggleMuteButton, boolean isMute)
    {
        StringBuilder newIconPath = isMute ? new StringBuilder("mute.png") : new StringBuilder("speaker.png");

        Image image = new Image(getClass().getResourceAsStream(newIconPath.insert(0, "/ui/icons/").toString()));
        updateButtonImage(toggleMuteButton, image);
    }

    public void handleShuffleModeButton(Button toggleShuffleModeButton, boolean shouldShuffle)
    {
        toggleButtonState(toggleShuffleModeButton, shouldShuffle);
    }

    public void handleRepeatModeButton(Button toggleShuffleModeButton, boolean shouldRepeat)
    {
        toggleButtonState(toggleShuffleModeButton, shouldRepeat);
    }

    private void toggleButtonState(Button button, boolean isActive)
    {
        if (isActive)
        {
            button.getStyleClass().add("buttonActive");
            button.getStyleClass().remove("buttonInactive");
        } else
        {
            button.getStyleClass().add("buttonInactive");
            button.getStyleClass().remove("buttonActive");
        }
    }

    private void updateButtonImage(Button button, Image image)
    {
        ImageView imageView = createImageViewWithDefaultValues(image);
        button.setGraphic(imageView);
    }

    private ImageView createImageViewWithDefaultValues(Image image)
    {
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(SMALL_BUTTON_SIZE);
        imageView.setFitWidth(SMALL_BUTTON_SIZE);
        imageView.setPickOnBounds(true);
        imageView.setPreserveRatio(true);
        return imageView;
    }

    // set each cell's height and font size to be bigger and more touch-screen friendly
    public void fixCellSize(ListView<String> songList)
    {
        songList.setCellFactory(list ->
                new ListCell<String>()
                {
                    @Override
                    protected void updateItem(String item, boolean empty)
                    {
                        super.updateItem(item, empty);
                        if (item != null)
                        {
                            setText(item);
                            setPrefHeight(40.0);
                            setFont(Font.font(16));
                        }
                    }
                });
    }
}
