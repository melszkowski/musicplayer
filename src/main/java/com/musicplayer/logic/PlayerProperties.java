package com.musicplayer.logic;

public class PlayerProperties
{
    public boolean shouldShuffle;
    public boolean shouldRepeat;
    public boolean shouldBePaused;
    public boolean shouldBeMute;

    public double volume;
}
