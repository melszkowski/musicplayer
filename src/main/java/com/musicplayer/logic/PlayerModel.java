package com.musicplayer.logic;

import com.musicplayer.Configuration;
import com.musicplayer.events.*;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import javafx.beans.value.ChangeListener;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PlayerModel
{

    public Subject<VisitableEvent> currentEvent;

    private List<String> songs;
    private MediaPlayer mediaPlayer;
    private List<Observer> observers;

    private ChangeListener<Duration> songProgressListener;
    private PlayerProperties playerProperties;

    public PlayerModel()
    {
        currentEvent = PublishSubject.create();

        songs = obtainListOfSongs();
        observers = new ArrayList<>();
        playerProperties = new PlayerProperties();

        playerProperties.shouldRepeat = false;
        playerProperties.shouldShuffle = false;
        playerProperties.shouldBePaused = false;
        playerProperties.volume = 100;
        playerProperties.shouldBeMute = false;
    }

    public List<String> getSongs()
    {
        return songs;
    }

    private List<String> obtainListOfSongs()
    {
        List<String> listOfSongs = new ArrayList<>();

        File[] files = new File(Configuration.MEDIA_PATH).listFiles();
        for (File file : files)
        {
            if (file.isFile())
            {
                listOfSongs.add(file.getName());
            }
        }
        return listOfSongs;
    }

    public void setSongProgressListener(ChangeListener<Duration> songProgressListener)
    {
        this.songProgressListener = songProgressListener;
    }

    public void playSong(String title)
    {
        if (title == null)
        {
            return;
        }

        stopCurrentSong();

        Media song = new Media(new File(Configuration.MEDIA_PATH + File.separator + title).toURI().toString());
        mediaPlayer = new MediaPlayer(song);

        applyVolume();
        applyMuteMode();

        mediaPlayer.play();
        setEvent(new MusicPlayEvent());


        mediaPlayer.setOnEndOfMedia(new Runnable()
        {
            @Override
            public void run()
            {
                setEvent(new SongEndEvent());
                mediaPlayer.currentTimeProperty().removeListener(songProgressListener);
            }
        });
        applyCurrentTimeListener();

    }

    public void continueSong()
    {
        if (mediaPlayer != null)
        {
            mediaPlayer.play();
            setEvent(new MusicPlayEvent());
        }

    }

    public void stopCurrentSong()
    {
        if (mediaPlayer != null && (mediaPlayer.getStatus().equals(MediaPlayer.Status.PLAYING) || mediaPlayer.getStatus().equals(MediaPlayer.Status.PAUSED)))
        {
            mediaPlayer.stop();
            setEvent(new MusicStopEvent());
            mediaPlayer.currentTimeProperty().removeListener(songProgressListener);
            mediaPlayer.dispose();
        }
    }

    public void pauseCurrentSong()
    {
        if (mediaPlayer != null && mediaPlayer.getStatus().equals(MediaPlayer.Status.PLAYING))
        {
            mediaPlayer.pause();
            setEvent(new MusicPauzeEvent());
        }
    }

    public void togglePlay(String selectedSong)
    {

        // if nothing being played, play new song
        // if paused (or stopped), continue playing (or play from the beginning)
        // if played, pause
        if (mediaPlayer == null)
        {
            playerProperties.shouldBePaused = false;
            playSong(selectedSong);
        } else if (mediaPlayer.getStatus().equals(MediaPlayer.Status.PAUSED))
        {
            playerProperties.shouldBePaused = false;
            continueSong();
        } else if (mediaPlayer.getStatus().equals(MediaPlayer.Status.STOPPED))
        {
            playSong(selectedSong);
        } else if (mediaPlayer.getStatus().equals(MediaPlayer.Status.PLAYING))
        {
            playerProperties.shouldBePaused = true;
            pauseCurrentSong();
        }
    }

    public void toggleMute()
    {
        playerProperties.shouldBeMute = !playerProperties.shouldBeMute;
        if (mediaPlayer != null)
        {
            mediaPlayer.setMute(playerProperties.shouldBeMute);
            sendMuteEvent();
        }
    }

    private void sendMuteEvent()
    {
        if (mediaPlayer.isMute())
            setEvent(new SoundOffEvent());
        else
            setEvent(new SoundOnEvent());
    }

    private void applyMuteMode()
    {
        mediaPlayer.setMute(playerProperties.shouldBeMute);
    }

    public void toggleShuffleMode()
    {
        playerProperties.shouldShuffle = !playerProperties.shouldShuffle;
        sendShuffleEvent();
    }

    private void sendShuffleEvent()
    {
        if (playerProperties.shouldShuffle)
            setEvent(new ShuffleOnEvent());
        else
            setEvent(new ShuffleOffEvent());

    }

    public void toggleRepeatMode()
    {
        playerProperties.shouldRepeat = !playerProperties.shouldRepeat;
        sendRepeatEvent();
    }

    private void sendRepeatEvent()
    {
        if (playerProperties.shouldRepeat)
            setEvent(new RepeatOnEvent());
        else
            setEvent(new RepeatOffEvent());
    }


    public void setVolume(double value)
    {
        playerProperties.volume = value;
        mediaPlayer.setVolume(value);
    }

    private void applyVolume()
    {
        mediaPlayer.setVolume(playerProperties.volume);
    }

    public double getCurrentSongTime() throws NullPointerException
    {
        return mediaPlayer.getCurrentTime().toMillis();
    }

    public double getSongDuration() throws NullPointerException
    {
        return mediaPlayer.getTotalDuration().toMillis();
    }

    public void setSongProgress(double percentage)
    {
        Duration d = new Duration(percentage * getSongDuration() / 100);
        mediaPlayer.seek(d);
    }

    public void disableSongBarProgressing()
    {
        removeCurrentTimeListener();
    }

    public void enableSongBarProgressing()
    {
        applyCurrentTimeListener();
    }

    private void applyCurrentTimeListener()
    {
        if (mediaPlayer != null && songProgressListener != null)
            mediaPlayer.currentTimeProperty().addListener(songProgressListener);
    }

    private void removeCurrentTimeListener()
    {
        if (mediaPlayer != null && songProgressListener != null)
            mediaPlayer.currentTimeProperty().removeListener(songProgressListener);
    }

    private void setEvent(VisitableEvent ve)
    {
        System.out.println("Setting new event, type: " + ve.toString());
        currentEvent.onNext(ve);
    }


}
