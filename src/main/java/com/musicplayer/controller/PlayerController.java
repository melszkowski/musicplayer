package com.musicplayer.controller;

import com.musicplayer.events.*;
import com.musicplayer.logic.PlayerModel;
import com.musicplayer.ui.PlayerScreenUpdater;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;

import java.net.URL;
import java.util.ResourceBundle;

public class PlayerController implements Initializable, EventVisitor, Observer
{

    private PlayerModel playerModel;
    private PlayerScreenUpdater playerScreenUpdater;

    @FXML
    private ListView<String> songList;

    @FXML
    private Button togglePlay, toggleMute, shuffle, repeat;

    @FXML
    private Slider volumeSlider;

    @FXML
    private Slider songProgress;


    public PlayerController()
    {
        playerModel = new PlayerModel();

        playerScreenUpdater = new PlayerScreenUpdater();

        songList = new ListView<String>();
    }

    // handle other things that need to be set AFTER object was created and thus can not be performed in constructor
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {

        songList.setItems(FXCollections.observableArrayList(playerModel.getSongs()));

        playerScreenUpdater.fixCellSize(songList);

        // set behaviour when new song in selected on list
        // (whether by tapping on it or by "next/previous" buttons
        songList.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> playerModel.playSong(newValue));

        // set listener on song progress, so that we can show progress on progress bar
        playerModel.setSongProgressListener((observableValue, oldValue, newValue) ->
        {
            // calculate percentage of currently played song and set that value on progress bar
            songProgress.setValue(100.0 * playerModel.getCurrentSongTime() / playerModel.getSongDuration());
        });

        playerModel.currentEvent.subscribe(
                event -> notify(event),
                error -> error.printStackTrace()
        );

    }

    @FXML
    public void handleButtonPressed(ActionEvent actionEvent)
    {
        Button button = (Button) actionEvent.getSource();
        String buttonId = button.getId();
        switch (buttonId)
        {
            case "togglePlay":
                playerModel.togglePlay(songList.getSelectionModel().getSelectedItem());
                break;
            case "stop":
                playerModel.stopCurrentSong();
                break;
            case "next":
                moveToNextSong();
                break;
            case "previous":
                moveToPreviousSong();
                break;
            case "toggleMute":
                playerModel.toggleMute();
                break;
            case "shuffle":
                playerModel.toggleShuffleMode();
                break;
            case "repeat":
                playerModel.toggleRepeatMode();
                break;

        }

    }

    @FXML
    public void onVolumeSliderChanged()
    {
        playerModel.setVolume(volumeSlider.getValue());
    }

    @FXML
    // handle user's request to seek specific time in song
    // re-apply progress bar progressing, as it was disabled on mouse-press
    // see onProgressSliderMousePressed()
    public void onProgressSliderMouseReleased()
    {
        playerModel.setSongProgress(songProgress.getValue());
        playerModel.enableSongBarProgressing();
    }

    @FXML
    // disable progressing displayed on bar, as it's handle should not run away when held by user
    public void onProgressSliderMousePressed()
    {
        playerModel.disableSongBarProgressing();
    }

    // select previous song on visible list, selects last element if current is first
    private void moveToPreviousSong()
    {
        int previousSongIndex = getPreviousSongIndex();
        selectSongByIndex(previousSongIndex);
    }

    // select next song on visible list, selects first element if current is last
    private void moveToNextSong()
    {
        int nextSongIndex = getNextSongIndex();
        selectSongByIndex(nextSongIndex);
    }

    private int getNextSongIndex()
    {
        int currentSongIndex = songList.getSelectionModel().getSelectedIndex();
        return currentSongIndex + 1 < songList.getItems().size() ? currentSongIndex + 1 : 0;
    }

    private int getPreviousSongIndex()
    {
        int currentSongIndex = songList.getSelectionModel().getSelectedIndex();
        return currentSongIndex - 1 >= 0 ? currentSongIndex - 1 : 0;
    }

    private void selectSongByIndex(int songIndex)
    {
        songList.scrollTo(songIndex);
        songList.getFocusModel().focus(songIndex);
        songList.getSelectionModel().select(songIndex);
    }

    // event listener:
    @Override
    public void notify(VisitableEvent event)
    {
        event.accept(this);
        System.out.println("Controller received event: " + event.toString());
    }

    @Override
    public void visit(MusicPlayEvent event)
    {
        playerScreenUpdater.handleTogglePlayButton(togglePlay, true);
    }

    @Override
    public void visit(MusicPauzeEvent event)
    {
        playerScreenUpdater.handleTogglePlayButton(togglePlay, false);
    }

    @Override
    public void visit(SoundOnEvent event)
    {
        playerScreenUpdater.handleToggleMuteButton(toggleMute, false);
    }

    @Override
    public void visit(SoundOffEvent event)
    {
        playerScreenUpdater.handleToggleMuteButton(toggleMute, true);
    }

    @Override
    public void visit(ShuffleOnEvent shuffleOnEvent)
    {
        playerScreenUpdater.handleShuffleModeButton(shuffle, true);
    }

    @Override
    public void visit(ShuffleOffEvent shuffleOffEvent)
    {
        playerScreenUpdater.handleShuffleModeButton(shuffle, false);
    }

    @Override
    public void visit(RepeatOnEvent repeatOnEvent)
    {
        playerScreenUpdater.handleRepeatModeButton(repeat, true);
    }

    @Override
    public void visit(SongEndEvent songEndEvent)
    {
        // TODO handle repeating modes, should be handled in model though
        moveToNextSong();
    }

    @Override
    public void visit(MusicStopEvent musicStopEvent)
    {
        playerScreenUpdater.handleTogglePlayButton(togglePlay, false);
        songProgress.setValue(0);
    }

    @Override
    public void visit(NoneEvent noneEvent)
    {
        // might be set initially as default, do nothing
    }

    @Override
    public void visit(RepeatOffEvent repeatOffEvent)
    {
        playerScreenUpdater.handleRepeatModeButton(repeat, false);
    }


}



