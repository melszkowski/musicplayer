package com.musicplayer.events;

public interface Visitable
{
    void accept(EventVisitor visitor);
}
