package com.musicplayer.logic;

import com.musicplayer.controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application
{

    public static void main(String[] args)
    {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception
    {
        FXMLLoader fxmlLoader = null;
        Parent root = null;
        try
        {
            fxmlLoader = new FXMLLoader(getClass().getResource("/ui/menuScreen.fxml"));
            root = (Parent) fxmlLoader.load();

        } catch (IOException ex)
        {
            System.exit(1);
        }

        System.out.println();

        MainController mainController = fxmlLoader.getController();
        primaryStage.setScene(new Scene(root, 800, 300));
        mainController.setStage(primaryStage);

        primaryStage.setTitle("Music Player");
        primaryStage.show();
    }
}
